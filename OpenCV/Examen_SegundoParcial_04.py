#El siguiente codigo saca los frames(imgenes) de un video o convierte el video en una carpeta de imagenes
import cv2

capture = cv2.VideoCapture('1VIDEO-EXAMENSP-SEBASTIANESCOBEDOOSUNA.wmv')
cont = 0

#Devideoaframesimagen-en esta carpeta esta el archivo py y el video
#frames-carpeta que ya debe de existir porque en esta se guardaran las imagenes
path = 'Resources\Frames'


while (capture.isOpened()):
    ret, frame = capture.read()
    if (ret == True):
        cv2.imwrite(path + '\\' 'IMG_%04d.jpg' % cont, frame)
        cont += 1
        if (cv2.waitKey(1) == ord('s')):
            break
    else:
        break


capture.release()
cv2.destroyAllWindows()
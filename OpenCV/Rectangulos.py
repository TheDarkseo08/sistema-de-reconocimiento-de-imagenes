import cv2
import numpy as np

imagen = 255 * np.ones((400,600,3), dtype = np.uint8)

#Rectangulo
cv2.rectangle(imagen, (0,0), (600,900), (0,0,0), -1)

cv2.rectangle(imagen, (50,50), (350,350), (255,51,51), 3)
cv2.rectangle(imagen, (400,20), (580,250), (51,255,51), 3)
cv2.rectangle(imagen, (430,60), (548,220), (255,255,55), 3)
cv2.circle(imagen, (450,330), 60, (79,79,233), -1)
cv2.rectangle(imagen, (100,100), (240,170), (51,153,255), -1)
cv2.rectangle(imagen, (100,250), (240,320), (102,255,255), -1)
cv2.rectangle(imagen, (180,200), (330,210), (255,102,255), -1)

cv2.imshow('Rectangulos', imagen)
cv2.imwrite('Rectangulos.png', imagen)
cv2.waitKey(0)
cv2.destroyAllWindows()
import cv2
import numpy as np

imagen = 255*np.ones((400,700,3),dtype=np.uint8)

font = cv2.FONT_HERSHEY_COMPLEX = 1
cv2.putText(imagen,'Practicando con OpenCV',(20,40),font,3,(0,0,0),2,cv2.LINE_AA)

cv2.imshow('Texto en OpenCV',imagen)

cv2.waitKey(0)
cv2.destroyAllWindows()

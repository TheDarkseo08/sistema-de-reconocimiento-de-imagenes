import cv2

#leer las imagenes
imagen1 = cv2.imread('Resources\Imagenes Horizontal\Captura_AlmacenandoRostros.png')
imagen2 = cv2.imread('Resources\Imagenes Horizontal\Captura_ConcatenarImagenes.png')
imagen3 = cv2.imread('Resources\Imagenes Horizontal\Captura_ImagenGris.png')
imagen4 = cv2.imread('Resources\Imagenes Horizontal\IMG_0008.jpg')
imagen5 = cv2.imread('Resources\Imagenes Horizontal\IMG_0012.jpg')
imagen6 = cv2.imread('Resources\Imagenes Horizontal\Captura_ImagenATexto.png')

#imprimir las medidas de las imagenes deberan medir lo mismo para concatenar
print('imagen1.shape = ', imagen1.shape)
print('imagen2.shape = ', imagen2.shape)

#concatenar horizontal y vertical
concatena_horizontal = cv2.hconcat([imagen1, imagen2,imagen3,imagen4,imagen5,imagen6])

#mostrar el resultado
cv2.imshow('concatena_horizontal', concatena_horizontal)

#guardar el resultado
cv2.imwrite('concatenahorizontal.png',concatena_horizontal)


cv2.waitKey(0)
cv2.destroyAllWindows()

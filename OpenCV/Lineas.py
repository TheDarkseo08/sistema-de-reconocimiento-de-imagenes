import cv2
import numpy as np

imagen = 255 * np.ones((400,600,3), dtype = np.uint8)

#linea
cv2.line(imagen, (150,150), (900,600), (160,160,160), 7)
cv2.line(imagen, (0,100), (100,0), (255,0,255), 6)
cv2.line(imagen, (0,170), (170,0), (0,255,0), 10)
cv2.line(imagen, (0,250), (250,0), (0,0,255), 4)
cv2.line(imagen, (0,300), (300,0), (255,255,204), 10)
cv2.line(imagen, (0,370), (370,0), (255,0,0), 4)
cv2.line(imagen, (0,450), (450,0), (0,0,255), 15)

cv2.imshow('Lineas', imagen)
cv2.imwrite('Lineas.png', imagen)
cv2.waitKey(0)
cv2.destroyAllWindows()
import cv2
faceClassif = cv2.CascadeClassifier(cv2.data.haarcascades+'haarcascade_frontalface_default.xml')
image1 = cv2.imread('Resources\Personas\Varias_Personas_03.jpg')

imageAux1 = image1.copy()

gray1 = cv2.cvtColor(image1, cv2.COLOR_BGR2GRAY)
faces1 = faceClassif.detectMultiScale(gray1, 1.2, 7)

count = 0
for (x,y,w,h) in faces1:
    cv2.rectangle(image1, (x,y),(x+w,y+h),(128,0,255),2)
    rostro1 = imageAux1[y:y+h,x:x+w]
    rostro1 = cv2.resize(rostro1,(150,150), interpolation=cv2.INTER_CUBIC)
    cv2.imwrite('rostro1_{}.jpg'.format(count),rostro1)
    count = count + 1
    cv2.imshow('rostro',rostro1)
    cv2.imshow('image',image1)
    cv2.waitKey(0)
cv2.destroyAllWindows()

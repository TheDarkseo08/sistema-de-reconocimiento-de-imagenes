import cv2
import imutils

#leer las imagenes
imagen1 = cv2.imread('./Resources/Emojis/Enojado.png')
imagen2 = cv2.imread('./Resources/Emojis/Feliz.png')
imagen3 = cv2.imread('./Resources/Emojis/Sorprendido.png')
imagen4 = cv2.imread('./Resources/Emojis/Triste.png')

#imprimir las medidas de las imagenes deberan medir lo mismo para concatenar
print('imagen1.shape = ', imagen1.shape)
print('imagen2.shape = ', imagen2.shape)

#concatenar horizontal y vertical
concatena_horizontal = cv2.hconcat([imagen1, imagen2,imagen2,imagen1])
concatena_vertical = cv2.vconcat([imagen2, imagen1,imagen1,imagen2])

#mostrar el resultado
cv2.imshow('concatena_horizontal', concatena_horizontal)
cv2.imshow('concatena_vertical', concatena_vertical)

#guardar el resultado
cv2.imwrite('concatenahorizontal.png',concatena_horizontal)
cv2.imwrite('concatenavertical.png',concatena_vertical)

cv2.waitKey(0)
cv2.destroyAllWindows()

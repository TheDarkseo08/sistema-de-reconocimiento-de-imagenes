import pytesseract as tess
from PIL import Image
import cv2
tess. pytesseract. tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

image = cv2.imread('Resources\codigodeimagenatexto.png')
txt = tess.image_to_string(image)

#puedes cambiar la posicion del texto y el color, en la siguiente linea estan los parametros
#cv2.putText(la imagen,el texto,la posicion del texto,tipo de letra,tamaño de letra,color,gruesor de la letra)
cv2.putText(image,txt,(30,30),cv2.FONT_HERSHEY_SIMPLEX,1,(255,120,0),1)

cv2.imshow('Imagen', image)
cv2.waitKey(0)
cv2.destroyAllWindows()
#Codigo para crear el 1videoexamensp de una por una las imagenes, en este ejemplo solo puse 5 pero deben ser mas

#Impotar cv2
import cv2

#Leer imagenes
img1 = cv2.imread('.\Resources\Examen\image0.png')
img2 = cv2.imread('.\Resources\Examen\image1.png')
img3 = cv2.imread('.\Resources\Examen\image2.png')
img4 = cv2.imread('.\Resources\Examen\image3.png')
img5 = cv2.imread('.\Resources\Examen\image4.png')
img6 = cv2.imread('.\Resources\Examen\image5.png')
img7 = cv2.imread('.\Resources\Examen\image6.png')
img8 = cv2.imread('.\Resources\Examen\image7.png')
img9 = cv2.imread('.\Resources\Examen\image8.png')
img10 = cv2.imread('.\Resources\Examen\image9.png')
img11 = cv2.imread('.\Resources\Examen\image10.png')
img12 = cv2.imread('.\Resources\Examen\image11.png')
img13 = cv2.imread('.\Resources\Examen\image12.png')
img14 = cv2.imread('.\Resources\Examen\image13.png')
img15 = cv2.imread('.\Resources\Examen\image14.png')
img16 = cv2.imread('.\Resources\Examen\image15.png')
img17 = cv2.imread('.\Resources\Examen\image16.png')
img18 = cv2.imread('.\Resources\Examen\image17.png')
img19 = cv2.imread('.\Resources\Examen\image18.png')
img20 = cv2.imread('.\Resources\Examen\image19.png')
img21 = cv2.imread('.\Resources\Examen\image20.png')
img22 = cv2.imread('.\Resources\Examen\image21.png')

#Tamaño de la última imagen alto y ancho
height, width  = img22.shape[:2]


#Caracteristicas del video
video = cv2.VideoWriter('1VIDEO-EXAMENSP-SEBASTIANESCOBEDOOSUNA.wmv',cv2.VideoWriter_fourcc(*'mp4v'),1,(width,height))

# Poner cada frame en video
video.write(img1)
video.write(img2)
video.write(img3)
video.write(img4)
video.write(img5)
video.write(img6)
video.write(img7)
video.write(img8)
video.write(img9)
video.write(img10)
video.write(img11)
video.write(img12)
video.write(img13)
video.write(img14)
video.write(img15)
video.write(img16)
video.write(img17)
video.write(img18)
video.write(img19)
video.write(img20)
video.write(img21)
video.write(img22)

#liberar recursos
video.release()
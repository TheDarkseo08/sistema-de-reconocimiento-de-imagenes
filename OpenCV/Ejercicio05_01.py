import cv2
import numpy as np
import imutils
image = cv2.imread('./OpenCV/Resources/OpenCV.png')

ancho = image.shape[1] #columnas
alto = image.shape[0] #filas
print("Columnas", ancho)
print("Filas", alto)

#Traslacion
M = np.float32([[1,0,50],[0,1,50]])
imageOut = cv2.warpAffine(image, M, (ancho, alto))

#Rotacion
M = cv2.getRotationMatrix2D((ancho//2, alto//2), 25, 1)
imageOut = cv2.warpAffine(image, M, (ancho, alto))

#Escalando una imagen
imageOut = cv2.resize(image, (600,600), interpolation = cv2.INTER_CUBIC)

#Escalando una imagen
imageOut1 = imutils.resize(image, width = 300)
imageOut2 = imutils.resize(image, height = 300)

#Recortar una imagen
imageOut = image[50:120, 165:250]

cv2.imshow('Imagen de entrada', image)
cv2.imshow('Imagen de salida', imageOut)
cv2.waitKey(0)
cv2.destroyAllWindows()








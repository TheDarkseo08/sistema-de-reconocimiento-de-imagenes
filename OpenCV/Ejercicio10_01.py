import cv2
imagen = cv2.imread('./Resources/Lucca.png')

flip01 = cv2.flip(imagen,0)
flip02 = cv2.flip(imagen,1)
flip03 = cv2.flip(imagen,-1)

cv2.imshow('imagen',imagen)
cv2.imshow('flip01',flip01)
cv2.imshow('flip02',flip02)
cv2.imshow('flip03',flip03)

cv2.waitKey(0)
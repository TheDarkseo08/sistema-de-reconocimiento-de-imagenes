import cv2
print("LA IMPORTACION de OPENCV SE HIZO CORRECTAMENTE, mostrar varias imagenes ")

img1= cv2.imread("./OpenCV/Resources/OpenCV.png")
img2= cv2.imread("./OpenCV/Resources/Grises.png")
img3= cv2.imread("./OpenCV/Resources/CapturaFigurasGeometricas.png",0)

print(img1.shape)
cv2.imshow("MOSTRAR IMAGEN UNO",img1) 
cv2.waitKey(10000)

print(img2.shape)
cv2.imshow("MOSTRAR IMAGEN DOS",img2)
cv2.waitKey(12000)

print(img3.shape)
cv2.imshow("MOSTRAR IMAGEN TRES",img3)
cv2.waitKey(11000)

cv2.destroyAllWindows()
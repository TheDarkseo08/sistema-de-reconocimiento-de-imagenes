import cv2
import numpy as np

imagen = 255 * np.ones((400,600,3), dtype = np.uint8)

#Circulo
cv2.circle(imagen, (100,80), 60, (79,79,233), -1)
cv2.circle(imagen, (200,200), 60, (10,233,233), -1)
cv2.circle(imagen, (280,330), 60, (79,79,233), -1)
cv2.circle(imagen, (380,150), 60, (94,232,99), -1)
cv2.circle(imagen, (450,330), 60, (10,233,233), -1)
cv2.circle(imagen, (45,180), 30, (94,232,99), -1)
cv2.circle(imagen, (90,300), 30, (232,94,186), -1)
cv2.circle(imagen, (250,50), 30, (232,94,186), -1)
cv2.circle(imagen, (480,50), 30, (94,232,99), -1)

cv2.imshow('Circulos', imagen)
cv2.imwrite('Circulos.png', imagen)
cv2.waitKey(0)
cv2.destroyAllWindows()
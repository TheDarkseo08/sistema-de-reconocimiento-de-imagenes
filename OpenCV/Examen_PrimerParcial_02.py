import cv2

captura = cv2.VideoCapture('./OpenCV/Resources/EVIDENCIA_LECTURADEIMAGENES.wmv')

while (captura.isOpened()):
    ret, imagen = captura.read()
    if ret == True:
        cv2.imshow('Video de las Imagenes para Examen', imagen)
        if cv2.waitKey(30) == ord('s'): 
            break
    else:break

captura.release()
cv2.destroyAllWindows()